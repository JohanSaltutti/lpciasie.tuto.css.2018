# Tutoriel LP CIASIE PostCss & Autoprefixer - Installation 

*L'utilisation de PostCss avec Autoprefixer peut se faire de différentes manières, mais il faut savoir que seule la dernière solution (l'utilisation avec un task runner) permet d'utiliser pleinement PostCss et ses plugins, car c'est la seule manière qui permet de modifier les paramètres des plugins. Heuresement l'installation n'est pas si compliquée et elle est détaillée à la commande près !* 

Passons donc en revue les différentes façon d'utiliser PostCss et autoprefixer (à noter que PostCss seul ne sert à rien sans au moins un plugin)

## En ligne

### Sur le site d'Autoprefixer

[Le site d'Autoprefixer](https://autoprefixer.github.io/) permet de le tester directement en ligne, il suffit d'entrer le code à préfixer et le tour est joué 

### Sur Codepen

Il est possible d'utiliser  postcss et autoprefixer à partir [de Codepen](https://codepen.io/), pour cela, se rendre dans les paramètres du css , choisir PostCSS en Css preprocessor et cocher Autoprefixer dans vendor prefixing

![codepen](http://image.noelshack.com/fichiers/2018/43/2/1540319000-img.png)

## Avec NodeJs

### En ligne de commande

Commencez par installer ou mettre à jour NodeJS (cela est nécessaire sur les machines de l'iut)

D'abord il faut initialiser un projet node

	npm init

Il faut ensuite installer PostCss

	npm i -g postcss-cli

Puis Autoprefixer
	
	npm i -g autoprefixer

Créez un fichier input.css et remplissez le par exemple avec :

	.exemple {
    	display: grid;
    	transition: all .5s;
    	user-select: none;
    	background: linear-gradient(to bottom, white, black);
	}

Ensuite utilisez la commande suivante pour préfixer le css

	cat input.css |  postcss -u "autoprefixer"  > output.css

### Avec un task runner

D'abord il faut initialiser un projet node
	
	npm init

Puis installer gulp en suivant ces deux commandes :
	
	npm install -g gulp
	npm install --save-dev gulp

Installation de postcss et d'autoprefixer :
	
	npm i gulp-postcss -D
	npm i autoprefixer -D
	

Créez un fichier gulpfile.js contenant : 

	let gulp = require('gulp')
	let postcss = require('gulp-postcss')

	//Liste des plugins à utiliser
	let processorArray = [
	    require('autoprefixer')({grid: true, browsers: ['>1%']})    //Ajout du plugin autoprefixer et de ses options
	];

	gulp.task('styles', function () {
	    return gulp.src('input/style.css')    //Dossier d'entrée
	        .pipe(postcss(processorArray))
	        .pipe(postcss([]))
	        .pipe(gulp.dest('output/'))    //Dossier de sortie
	});

Et voilà, maintenant vous n'avez plus qu'a placer un fichier style.css dans un dossier input et de créer un dossier output où apparaitra votre css préfixé, pour le faire apparaître tapez simplement la commande suivante :
	gulp style


#### Rajouter des plugins

L'avantage de gulp est qu'il est facile de rajouter de nouveau plugin. Il suffit pour cela de l'installer avec npm, puis de le require dans preprocessorArray de la même manière que autoprefixer. Il existe des dizaines de plugin pour vous aider à programmer en css. PostCss, essayez les tous ! 
